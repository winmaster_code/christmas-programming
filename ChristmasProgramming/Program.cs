﻿namespace ChristmasProgramming
{
    public static class Program
    {
        static void Main(string[] args)
        {
            ASCIIarts.GenerateMerryChristmasArt();
            ASCIIarts.ColorfulAnimation();
            ASCIIarts.GenerateBlinkingTreeArt();
        }
    }
}
