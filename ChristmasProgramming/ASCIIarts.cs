﻿using Colorful;
using System.Drawing;
using System.Threading;
using Console = Colorful.Console;

namespace ChristmasProgramming
{
    public static class ASCIIarts
    {
        public static void GenerateMerryChristmasArt()
        {
            Console.WriteAscii("Merry Christmas !!!", Color.Red);
        }

        public static void GenerateBlinkingTreeArt()
        {
            StyleSheet styleSheet = new StyleSheet(Color.White);
            styleSheet.AddStyle(@"/", Color.Green);
            styleSheet.AddStyle(@"\\", Color.Green);
            styleSheet.AddStyle(@"=", Color.GreenYellow);
            styleSheet.AddStyle(@"O", Color.BlueViolet);
            styleSheet.AddStyle(@"\*", Color.Cyan);
            styleSheet.AddStyle(@"i", Color.Orange);

            var consoleTop = Console.CursorTop;
            var consoleLeft = Console.CursorLeft;

            for (int i = 0; i < 100; i++)
            {
                Console.SetCursorPosition(consoleLeft, consoleTop);
                Console.WriteLine("\n\n");
                      Console.WriteLine(@"         |         ", i % 2 == 0 ? Color.DarkOrange : Color.LightGoldenrodYellow);
                      Console.WriteLine(@"        -+-        ", i % 2 == 0 ? Color.OrangeRed : Color.LightGoldenrodYellow);
                      Console.WriteLine(@"         ^         ", i % 2 == 0 ? Color.Orange : Color.LightGoldenrodYellow);
                Console.WriteLineStyled(@"        /=\        ", styleSheet);
                Console.WriteLineStyled(@"      i/ O \i      ", styleSheet);
                Console.WriteLineStyled(@"      /=====\      ", styleSheet);
                Console.WriteLineStyled(@"      /  i  \      ", styleSheet);
                Console.WriteLineStyled(@"    i/ O * O \i    ", styleSheet);
                Console.WriteLineStyled(@"    /=========\    ", styleSheet);
                Console.WriteLineStyled(@"    /  *   *  \    ", styleSheet);
                Console.WriteLineStyled(@"  i/ O   i   O \i  ", styleSheet);
                Console.WriteLineStyled(@"  /=============\  ", styleSheet);
                Console.WriteLineStyled(@"  /  O   i   O  \  ", styleSheet);
                Console.WriteLineStyled(@"i/ *   O   O   * \i", styleSheet);
                Console.WriteLineStyled(@"/=================\", styleSheet);
                      Console.WriteLine(@"       |___|       ", Color.SaddleBrown);

                Thread.Sleep(120);
            }
        }

        public static void ColorfulAnimation()
        {
            var consoleTop = Console.CursorTop;
            var consoleLeft = Console.CursorLeft;

            for (int i = 0; i < 40; i++)
            {
                Console.SetCursorPosition(consoleLeft, consoleTop);

                Console.Write("       . . . . Ho, Ho, Ho o", Color.LightGray);
                for (int s = 0; s < i / 2; s++)
                {
                    Console.Write(" o", Color.LightGray);
                }
                Console.WriteLine();

                var margin = "".PadLeft(i);
                Console.WriteLine(margin + "                _____      o", Color.LightGray);
                Console.WriteLine(margin + "       ____====  ]OO|_n_n__][.", Color.DeepSkyBlue);
                Console.WriteLine(margin + "      [________]_|__|________)< ", Color.DeepSkyBlue);
                Console.WriteLine(margin + "       oo    oo  'oo OOOO-| oo\\_", Color.DarkRed);
                Console.WriteLine("   +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+", Color.Silver);

                Thread.Sleep(120);
            }
        }
    }
}
